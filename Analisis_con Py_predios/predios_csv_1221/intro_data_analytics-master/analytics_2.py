#Script to analice a dataset from Bogota`s OpenData Hub.

# 1. Download the source dataset in csv format.
# https://datosabiertos.bogota.gov.co/dataset/predios-bogota

#2. Load the packages that we need for the analysis

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#2. Load the data using read_csv from pandas
#miData=pd.read_csv("predios_csv_1221.csv")
#myData=pd.read_csv("predios_csv_1221.csv", on_bad_lines='skip', delimiter=';',skiprows=[i for i in range(1,50000)], nrows=20000)
#myData=pd.read_csv("predios_csv_1221.csv", on_bad_lines='skip', delimiter=';',nrows=10000)
#2.A. Cuando el conjunto de datos es grande se recomienda colocar el tipo de dato que tiene cada columna
# Tip: el diccionario de dtype puede crearse al vuelo con un conjunto reducido de datos.

#Converters
#creating functions to clean the columns
def CPreAConst(x):
    x=x.replace(',','.')
    x=x.replace(' ','')
    x=round(float(x), 2)
    
    return x


myData=pd.read_csv("predios_csv_1221.csv", on_bad_lines='skip', delimiter=';',
converters={
    'PreAConst':CPreAConst
    }, 
nrows=1000)


for x in myData['PreAConst']:
    print(x)

